menu_juegos () {
     case $submenu in
        1)

           echo "Va a borrar las siguientes instancias: "
           
           # Da el nombre de los deploy de minecraft
           kubectl get deployment -l app=minecraft --no-headers -o custom-columns=":metadata.name"
           #kubectl get pods -l app=nextcloud -o=name
            
            sleep 10

           # Guardar salida de comando como variable.
           nombreinstancia=$(kubectl get deployment -l app=minecraft --no-headers -o custom-columns=':metadata.name')
           #Borra el Deployment
           kubectl delete deployment $nombreinstancia

            sleep 5

        ;;
        2)
           echo "Creando instancia deployment ..."

           kubectl apply -f https://gitlab.com/luisdieguezbarrio/opensource-hosting/-/raw/master/HOSTING/JUEGOS/Minecraft/MINE.yaml

           sleep 5

        ;;
        3)
           echo "Cargando estado ..."

            # Guardar salida de comando como variable.
           nombreinstancia=$(kubectl get deployment -l app=minecraft --no-headers -o custom-columns=':metadata.name')
           # ve el estado
           kubectl describe deployment $nombreinstancia

            read siguiente

        ;;
        *)
           echo "Numero no reconocido."
           sleep 1
        ;;
     esac
}

menu_almacenamiento () {
     case $submenu in
        1)

           echo "Va a borrar las siguientes instancias: "
           
           # Da el nombre de los deploy de Nextcloud
           kubectl get deployment -l app=nextcloud --no-headers -o custom-columns=":metadata.name"
           #kubectl get pods -l app=nextcloud -o=name
            
            sleep 10

           # Guardar salida de comando como variable.
           nombreinstancia=$(kubectl get deployment -l app=nextcloud --no-headers -o custom-columns=':metadata.name')
           #Borra el Deployment
           kubectl delete deployment $nombreinstancia

            sleep 5

        ;;
        2)
           echo "Creando instancia deployment ..."

           kubectl apply -f https://gitlab.com/luisdieguezbarrio/opensource-hosting/-/raw/master/HOSTING/ALMACENAMIENTO/Nextcloud/TODO.yaml

           sleep 5

        ;;
        3)
           echo "Cargando estado ..."

            # Guardar salida de comando como variable.
           nombreinstancia=$(kubectl get deployment -l app=nextcloud --no-headers -o custom-columns=':metadata.name')
           # ve el estado
           kubectl describe deployment $nombreinstancia

            read siguiente

        ;;
        *)
           echo "Numero no reconocido."
           sleep 1
        ;;
     esac
}

until [ "$seleccion" = "5" ]; do
   clear
   echo ""
   echo "Bienvenido al menú de control para OpenSource Hosting en Kubernetes. Por Luis Dieguez."
   echo "Recuerde ejecutar este script desde donde tenga acceso a su Cluster."
   echo "Test de que tiene instalado KUBECTL:"

    if ! [ -x "$(command -v kubectl)" ]; 
    then
        echo " * ERROR. No tiene instalado KUBECTL."
        echo "   No tener instalado esta herramienta puede significar que no esta tiene permisos para administrar el cluster"
    else
        echo " * Tiene correctamente instalado KUBECTL."
    fi

   echo "Ahora escriba el número correspondiente a la acción que quiera realizar:" 
   echo ""
   echo "1. ALMACENAMIENTO"
   echo "2. JUEGOS" 
   echo "3. " 
   echo "4. " 
   echo "" 
   echo "5. Salir del script. Exit." 
   echo ""
   echo -n "Seleccione una opción [1 - 5]: "
     read seleccion
     case $seleccion in
        1)
            clear
           echo "Menú de ALMACENAMIENTO. Elija una opción:"
           echo " 1. Borrar instancia deployment (NO DISCO)"
           echo " 2. Crear instancia deployment"
           echo " 3. Ver estado general"
           echo -n "Seleccione una opción [1 - 3]: "
           read submenu
            menu_almacenamiento

        ;;
        2)
            clear
           echo "Menú de JUEGOS. Elija una opción:"
           echo " 1. Borrar instancia deployment (NO DISCO)"
           echo " 2. Crear instancia deployment"
           echo " 3. Ver estado general"
           echo -n "Seleccione una opción [1 - 3]: "
           read submenu
            menu_juegos

        ;;
        3)
           echo "Descargando y ejecutando Script para DEBIAN"

        ;;
        4)
           echo "Descargando y ejecutando Script para CENTOS"

        ;;
        5)
           echo "Saliendo ..."
           exit
        ;;
        *)
           echo "Numero no reconocido."
           sleep 1
        ;;
     esac
done